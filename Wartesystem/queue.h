/* ------------------------------------------------------------------------- */
/* Datei:              queue.h                                               */
/* Autor:              Marcus Meiburg, Hannes Range                          */
/* Version:            1.4                                                   */
/* letzte Bearbeitung: 31.05.2013                                            */
/* Beschreibung:       Headerdatei der FIFO Warteschlange "queue.c"          */
/* ------------------------------------------------------------------------- */

#ifndef __QUEUE__
#define __QUEUE__

/* Includes */
#include <stdio.h>
#include <stdlib.h>

#define MAX_ACCESS 2

typedef enum {FALSE=0, TRUE} Boolean;


/* ------------------------------------------------------------------------- */
/*                 Repraesentation einer Zelle der Queue                     */
/* ------------------------------------------------------------------------- */
struct queue_zelle {
   unsigned pid;             // Prozess ID
   unsigned size;            // Prozess Groesse
   unsigned access;          // Anzahl der Zugriffe
   struct queue_zelle *next; // Zeiger auf die naechste Zelle
   struct queue_zelle *prev; // Zeiger auf die vorherige Zelle
};
typedef struct queue_zelle qZelle; 

/* ------------------------------------------------------------------------- */
/*                         Prototypenvereinbarung                            */
/* ------------------------------------------------------------------------- */
/* Initialiesierungs Funktion ACHTUNG!: muss als erstes Implementiert werden */
Boolean initQueue(void);
/* Prueft ob die Queue leer ist */
Boolean queueIsEmpty();
/* Versucht der Queue ein Element hinzuzufuegen */
Boolean enQueue(unsigned pid, unsigned size);
/* Gibt die pID des ersten Elements aus und loescht es aus der Queue */
unsigned deQueue();
/* Gibt das erste Element als struct zurueck */
qZelle *queueGetFirst();
void queueCellToFirst(qZelle* cell);
/* Gibt das Element mit der kleinsten Size zurueck */
qZelle *queueGetNext();
void queueFirstToEnd();
void qPrint();

#endif /* __QUEUE__ */