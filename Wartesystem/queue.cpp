/* ------------------------------------------------------------------------- */
/* Datei:              queue.cpp                                             */
/* Autor:              Marcus Meiburg, Hannes Range                          */
/* Version:            1.3                                                   */
/* letzte Bearbeitung: 31.05.2013                                            */
/* Beschreibung:       Implementation einer FIFO Warteschlange               */
/* ------------------------------------------------------------------------- */

//#include "globals.h"
#include "queue.h"

/* ------------------------------------------------------------------------- */
/*                       Globale Variablen                                   */
/* ------------------------------------------------------------------------- */
/* Anfang der Warteschlange */
qZelle *qAnfang;

/* ------------------------------------------------------------------------- */
/*                       Initialisierung                                    */
/* ------------------------------------------------------------------------- */
Boolean initQueue(void) {
  /* Mehrfaches Initialisieren vermeiden*/
  if(qAnfang == NULL) {
    if((qAnfang=(qZelle*)malloc(sizeof(qZelle))) != NULL)  {
      qAnfang->pid = 0;
      qAnfang->size = 0;
      qAnfang->prev = qAnfang;
      qAnfang->next = qAnfang;
      return TRUE;
    }
  }
  return FALSE;
}

/* Pruefft ob die Warteschlange leer ist*/
Boolean queueIsEmpty() {
  if(qAnfang->next == qAnfang)
    return TRUE;
  else
    return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                      Hinzufuegen einer Zelle                              */
/* ------------------------------------------------------------------------- */
Boolean enQueue(unsigned pid, unsigned size) {
  qZelle *neuZelle; // Neu einzufuegende Zelle

  if((neuZelle = (qZelle*)malloc(sizeof(qZelle))) != NULL) {
    //Zelle immer vor dem "anfang" einfuegen
    neuZelle->prev = qAnfang->prev;
    neuZelle->prev->next = neuZelle;
    neuZelle->next = qAnfang;
    qAnfang->prev = neuZelle;
    neuZelle->pid = pid;
    neuZelle->size = size;
    neuZelle->access = 0;
    return TRUE;
  }
  return FALSE;
}

/* ------------------------------------------------------------------------- */
/*                      Loeschen einer Zelle                                 */
/* ------------------------------------------------------------------------- */
unsigned deQueue() {
  unsigned pid;    // speichert die Pid der zu loeschende Zelle
  qZelle* helper;  // Zeiger auf die zu loeschende Zelle

  /* Wenn die Warteschlange nicht leer ist pid des erstes Elements ausgeben */
  if(!queueIsEmpty()) {
      // Zelle nach "anfang" auslesen und loeschen
      helper = qAnfang->next;
      pid = helper->pid;
      qAnfang->next = helper->next;
      helper->next->prev = qAnfang;
      free(helper);
      return pid;
  } else {
    //logGeneric("Warteschlange ist leer");
  }
  return 0;
}

/* ------------------------------------------------------------------------- */
/*            Gibt die beste Zelle zum einlagern zurueck                     */
/* ------------------------------------------------------------------------- */
/* Sucht eine Zelle die moeglichst klein ist und nicht zu lange gewartet hat */
/* ------------------------------------------------------------------------- */
qZelle *queueGetNext() {
  qZelle* zeiger = qAnfang->next;       /* zeiger zum durchwandern der Queue */
  qZelle* nextElement = qAnfang->next;  /* zeiger als Rueckgabe der Funktion */
  unsigned minSize = 9999999;           /* aktuelle minimale Size der Queue  */
  unsigned maxAccess = MAX_ACCESS;      /* aktueller maximaler Zugriff  ""   */
  Boolean check = TRUE;                 /* wurde eine Zelle gefunden ?       */

  if(!queueIsEmpty()) {
    /* Zugriff bei allem Elementen erhoehen */
    zeiger = qAnfang->next;
    while(zeiger != qAnfang) {
      zeiger->access++;
      zeiger = zeiger->next;
    }
    zeiger = qAnfang->next;
    while(zeiger != qAnfang) {
      /* Bestimmt die Zelle mit den meisten Zugriffen */
      if(zeiger->access > maxAccess) {
          maxAccess = zeiger->access;
          nextElement = zeiger;
          check = FALSE;
          if(nextElement != qAnfang->next) {
            queueCellToFirst(nextElement);
          }
      /* Bestimme die kleinste Zelle wenn nicht schon eine */
      /* mit mehr Zugriffen gefunden wurde                 */
      } else if(zeiger->size < minSize && check) {
          minSize = zeiger->size;
          nextElement = zeiger;
          if(nextElement != qAnfang->next) {
            queueCellToFirst(nextElement);
          }
      }
      zeiger = zeiger->next;
    }
  }
  /* Gibt die erste Zelle zurueck falls es kein Ergebnis gibt */
  return nextElement;
}

/* ------------------------------------------------------------------------- */
/*            Schiebt eine Zelle an den Anfang der Queue                     */
/* ------------------------------------------------------------------------- */
void queueCellToFirst(qZelle* cell) {
  qZelle* vorgaenger = cell->prev;
  qZelle* nachfolger = cell->next;

  cell->next = qAnfang->next;
  qAnfang->next->prev = cell;
  qAnfang->next = cell;
  cell->prev = qAnfang;

  vorgaenger->next = nachfolger;
  if(nachfolger != NULL)
    nachfolger->prev = vorgaenger;
}

/* ------------------------------------------------------------------------- */
/*            Schiebt die erste Zelle an das Ende der Queue                  */
/* ------------------------------------------------------------------------- */
void queueFirstToEnd() {
  qZelle* first = qAnfang->next;
  qZelle* last = qAnfang->prev;

  if(!queueIsEmpty()) {
    first->prev = last;
    first->prev->next = first;
    qAnfang->next = first->next;
    first->next->prev = qAnfang;
    first->next = qAnfang;
    qAnfang->prev = first;
  }
}
/* ------------------------------------------------------------------------- */
/*            Gibt das erste Element der Queue als Struktur aus              */
/* ------------------------------------------------------------------------- */
qZelle *queueGetFirst() {
  if(!queueIsEmpty()) {
    return qAnfang->next;
  }
  return NULL;
}

int main(int argc, char* argv[]) {
  char dummy[81];
  printf("Program started...\n\n");
  if(initQueue()) {
    enQueue(4,1000);
    //queueGetNext();
    enQueue(5,1000);
    queueGetNext();
    queueGetNext();
    enQueue(7,825);
    qPrint();
    printf("Prozess %u die meisten Zugriffe\n",queueGetNext()->pid);
    qPrint();
    //queueGetFirst()->access = 4;

  }
  printf("...program finished!\n");
  gets(dummy);
  return 0;
}

/* Gibt alle Speicherzellen in einer Tabelle aus*/
void qPrint() {
  qZelle* zeiger = qAnfang->next;
  printf("------------------------\n");
  printf("| PID |  Size | Access |\n");
  printf("------------------------\n");
  while(zeiger != qAnfang) {
    printf("| %3u | %5u | %6u |\n",zeiger->pid, zeiger->size,zeiger->access);
    zeiger = zeiger->next;
  }
  printf("------------------------\n");
}